# [pi](https://gitlab.com/eidoom/pi)
* This is a basic Rosetta Stone
## Pi
* [OEIS: Pi](http://oeis.org/A000796/constant)
    ```
    3.14159265358979323846264338327950288419716939937510582097494459230781640628620899862803482534211706798214   
    ```
* <https://en.wikipedia.org/wiki/Approximations_of_%CF%80>
* <https://mathworld.wolfram.com/PiFormulas.html>
## Dependencies
### Fedora
`dnf` provides:
```shell
sudo dnf install bash make gcc gcc-c++ gcc-gfortran clang rust sbcl ghc golang java-latest-openjdk java-latest-openjdk-devel python3 zig lua luajit nodejs gcc-gdc ldc ocaml swift-lang erlang mono-core
```
The following are not in the Fedora repositories:
* [Nim](https://nim-lang.org/install_unix.html)
    * Install with `curl https://nim-lang.org/choosenim/init.sh -sSf | sh`
    * Update with `choosenim update stable`
    * `nix-env -iA nixpkgs.nim` is out of date
* [Janet](https://janet-lang.org/docs/index.html)
    * Install pre-built binary from <https://github.com/janet-lang/janet/releases>
    * `nix-env -iA nixpkgs.janet` is out of date
* [Odin](https://odin-lang.org/docs/install/)
    * Can use `nix-env -iA nixpkgs.odin`
* [Deno](https://deno.land/#installation)
    * Install with `curl -fsSL https://deno.land/install.sh | sh`
    * Update with `deno upgrade`
    * Alternatively `sudo snap install deno`
    * `nix-env -iA nixpkgs.deno` is out of date
## Implementation languages
### C
* <http://www.open-std.org/jtc1/sc22/wg14/>
* <https://clang.llvm.org/>
* <https://gcc.gnu.org/>
* <https://en.wikipedia.org/wiki/C_(programming_language)>
### C++
* <https://isocpp.org/>
* <https://en.cppreference.com/w/>
* <https://en.wikipedia.org/wiki/C%2B%2B>
### C#
* <https://en.wikipedia.org/wiki/C_Sharp_(programming_language)>
* <https://docs.microsoft.com/en-us/dotnet/csharp/>
* `mono`: <https://www.tutorialspoint.com/executing-chash-code-in-linux>
### Common Lisp
* <https://lisp-lang.org/>
* <http://www.sbcl.org/>
* <https://en.wikipedia.org/wiki/Common_Lisp>
### D
* <https://dlang.org/>
* <https://en.wikipedia.org/wiki/D_(programming_language)>
### Erlang
* <https://en.wikipedia.org/wiki/Erlang_(programming_language)>
* <https://www.erlang.org/>
### Fortran
* <https://fortran-lang.org/>
* <https://en.wikipedia.org/wiki/Fortran>
### Futhark
* <https://futhark-lang.org/>
### Go
* <https://golang.org/>
* <https://en.wikipedia.org/wiki/Go_(programming_language)>
### Haskell
* <https://www.haskell.org/>
* <http://learnyouahaskell.com/chapters>
* <https://www.haskell.org/ghc/>
* <https://en.wikipedia.org/wiki/Haskell_(programming_language)>
### Janet
* <https://janet-lang.org/>
### Java
* <https://java.com/en/>
* <https://openjdk.java.net/>
* <https://en.wikipedia.org/wiki/Java_(programming_language)>
* Ensure `javac` and `java` are the same version
    * Set `java` version with [`sudo alternatives --config java`](https://docs.fedoraproject.org/en-US/quick-docs/installing-java/#_switching_between_java_versions)
### JavaScript
* <https://www.ecma-international.org/technical-committees/tc39/>
* <https://nodejs.org/en/>
* <https://en.wikipedia.org/wiki/JavaScript>
#### TypeScript
* <https://www.typescriptlang.org/>
* <https://deno.land/>
### Julia
* <https://julialang.org/>
* <https://en.wikipedia.org/wiki/Julia_(programming_language)>
* Ensure `--cpu-target` set appropriately in `bench.toml`
* I think `julia` is doing a slow start-up/load/compile step every time it runs
    * It should compare more favourably at higher `n` if true
### Lua
* <https://www.lua.org/>
* <https://luajit.org/luajit.html>
* <https://en.wikipedia.org/wiki/Lua_(programming_language)>
### Nim
* <https://nim-lang.org/>
* <https://en.wikipedia.org/wiki/Nim_(programming_language)>
### OCaml
* <https://ocaml.org/>
* <https://en.wikipedia.org/wiki/OCaml>
### Odin
* <https://odin-lang.org/>
### Python
* <https://www.python.org/>
* <https://www.pypy.org/>
### Ruby
* <https://www.ruby-lang.org/en/>
* <https://en.wikipedia.org/wiki/Ruby_(programming_language)>
### Rust
* <https://www.rust-lang.org/>
* <https://en.wikipedia.org/wiki/Rust_(programming_language)>
### Swift
* <https://www.swift.org/>
* <https://en.wikipedia.org/wiki/Swift_(programming_language)>
### Zig
* <https://ziglang.org/>
* [Language reference](https://ziglang.org/documentation/master/)
* [Standard library reference](https://ziglang.org/documentation/master/std/)
* <https://ziglearn.org/>
* <https://en.wikipedia.org/wiki/Zig_(programming_language)>
