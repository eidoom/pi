# Newton / Euler Convergence Transformation / Arctangent

$ \pi = 2 sum_{n=0}^{\infty} \frac{2^{n}{n!}^2}{(2n+1)!}
