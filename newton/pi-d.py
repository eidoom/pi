#!/usr/bin/env python3

import sys, functools


@functools.lru_cache(maxsize=None)
def factorial(n):
    return n * factorial(n - 1) if n else 1


def newton(n):
    return 2 * sum(2 ** i * factorial(i) ** 2 / factorial(2 * i + 1) for i in range(n))


if __name__ == "__main__":
    print(f"{newton(int(sys.argv[1])):.17f}")
