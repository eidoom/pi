#!/usr/bin/env python3

import sys


def fac(n, cache):
    for i in range(len(cache), n + 1):
        cache.append(i * cache[i - 1])
    return cache[n]


def newton(n):
    cache = [1]
    t = 0
    for i in range(n):
        t += 2 ** i * fac(i, cache) ** 2 / fac(2 * i + 1, cache)
    return 2 * t


if __name__ == "__main__":
    print(f"{newton(int(sys.argv[1])):.17f}")
