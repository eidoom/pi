#!/usr/bin/env lua

function leibniz(n)
	local t = 0
	for i = 0, n - 1 do
		t = t + (i % 2 == 1 and -1 or 1) / ( 2 * i + 1)
	end
	return 4 * t
end

print(leibniz(arg[1]))
