#!/usr/bin/env bash

echo $(bc <<<"""
scale=17;
t = 0;
for (i = 0; i < $1; i++) {
    t += -1^i/(2*i+1);
}
print 4*t;
""")
