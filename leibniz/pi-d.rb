#!/usr/bin/env ruby

def pi n
  4.0 * (0 .. n - 1).reduce(0) { |acc, i|
    acc + (i % 2 == 1 ? -1.0 : 1.0) / (2.0 * i + 1.0)
  }
end

puts "#{'%.17f' % pi(ARGV[0].to_i)}"
