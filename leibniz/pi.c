#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

double pi(const int n)
{
    double t = 0.;

    for (int i = 0; i < n; ++i) {
        t += (i % 2 == 1 ? -1. : 1.) / (2. * i + 1.);
    }

    return 4. * t;
}

int main(int argc, char* argv[])
{
    assert(argc == 2);
    printf("%.17f\n", pi(atoi(argv[1])));
}
