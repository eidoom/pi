#!/usr/bin/env bash

run() {
    border
    echo $1
    TIMEFORMAT="%Rs" bash -c "time $2 $3"
}

border() {
    echo --------------------
}

header() {
    border
    echo "n      " $(numfmt --to=si $1)
}


make -j

n8=100000000
header $n8
run "c (gcc)" ./pi-c-gcc $n8
run "c (clang)" ./pi-c-clang $n8
run "fortran (gfortran)" ./pi-f90 $n8
run "c++ (gcc)" ./pi-cpp-gcc $n8
run "c++ (clang)" ./pi-cpp-clang $n8
run "declarative c++ (gcc)" ./pi-d-cpp-gcc $n8
run "declarative c++ (clang)" ./pi-d-cpp-clang $n8
run "rust" ./pi-rs $n8
run "declarative rust" ./pi-d-rs $n8
run "go" ./pi-go $n8
run "nim" ./pi-nim $n8
run "java (openjdk)" "java pi" $n8
run "python (pypy)" "pypy3 pi.py" $n8
run "declarative python (pypy)" "pypy3 pi.py" $n8
run "declarative julia" "julia pi-d.jl" $n8
run "julia" "julia pi.jl" $n8
run "haskell (ghc)" ./pi-hs $n8
run "javascript (node)" "node pi.js" $n8

n7=10000000
header $n7
run "declarative javascript (node)" "node pi-d.js" $n7
run "python (cpython)" "python3 pi.py" $n7
run "declarative python (cpython)" "python3 pi.py" $n7
run "janet" "janet pi.janet" $n7
run "declarative janet" "janet pi-d.janet" $n7
run "common lisp (sbcl)" "sbcl --script pi.lisp" $n7

n6=1000000
header $n6
run "common lisp (ecl)" "ecl --shell pi.lisp --" $n6
run "common lisp (clisp)" "clisp pi.lisp --" $n6
run "bash" "bash pi.sh" $n6

border
