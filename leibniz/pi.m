#!/usr/bin/env -S math -script

pi[n_] := 4 * Sum[If[Mod[i, 2] == 1, -1, 1] / (2 * i + 1), {i, 0, n - 1}];

Print @ N[#, 17]& @ pi @ ToExpression @ #[[4]]& @ $CommandLine;


