double pi(const int n)
{
    import std.range : iota;
    import std.algorithm.iteration : fold;

    return 4. * iota(0, n).fold!((acc, i) => acc + (i % 2 == 1 ? -1. : 1.) / (2. * cast(double) i + 1.))(0.);
}

void main(string[] args)
{
    import std.stdio : writefln;
    import std.conv : parse;

    assert(args.length == 2);
    writefln("%.17f", pi(args[1].parse!int));
}
