#!/usr/bin/env janet

(defn pi [n]
    (* 4 (reduce (fn [t i] (+ t (/ (if (= 1 (% i 2)) -1 1) (inc (* 2 i))))) 0 (range n))))

(print (pi (->> :args dyn 1 scan-number)))
