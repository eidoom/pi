#include <cassert>
#include <iostream>
#include <numeric>
#include <vector>

double pi(const int n)
{
    std::vector<int> is(n);
    std::iota(is.begin(), is.end(), 0);

    return 4. * std::accumulate(is.cbegin(), is.cend(), 0., [](double t, int i) -> double {
            return t + ((i % 2 == 1 ? -1. : 1.) / (2. * static_cast<double>(i) + 1.)); });
}

int main(int argc, char* argv[])
{
    std::cout.sync_with_stdio(false);
    std::cout.precision(18);

    assert(argc == 2);

    std::cout << pi(std::atoi(argv[1])) << '\n';
}
