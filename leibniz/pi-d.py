#!/usr/bin/env python3

import sys


def pi(n):
    return 4 * sum((-1 if i % 2 else 1) / (2 * i + 1) for i in range(n))


if __name__ == "__main__":
    print(f"{pi(int(sys.argv[1])):.17f}")
