public class pi {
    public static double pi(int n) {
        double t = 0.;

        for (int i = 0; i < n; ++i) {
            t += (i % 2 == 1 ? -1. : 1.) / (2. * i + 1.);
        }

        return 4. * t;
    }

    public static void main(String[] args) {
        System.out.printf("%.17f\n", pi(Integer.parseInt(args[0])));
    }
}
