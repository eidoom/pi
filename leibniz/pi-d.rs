fn pi(n: i32) -> f64 {
    4. * (0..n)
        .fold(0., |acc, i| acc + (if i % 2 == 1 { -1. } else { 1. }) / (2. * (i as f64) + 1.))
}

fn main() {
    println!(
        "{:.17}",
        pi(std::env::args()
            .nth(1)
            .expect("No argument!")
            .parse::<i32>()
            .expect("Couldn't parse argument!"))
    );
}
