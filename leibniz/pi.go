package main

import (
	"fmt"
	"os"
	"strconv"
)

func pi(n int) float64 {
	var t float64 = 0
	for i := 0; i < n; i++ {
		var s float64
		if i%2 == 1 {
			s = -1
		} else {
			s = 1
		}

		t += s / (2*float64(i) + 1)
	}
	return 4 * t
}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Incorrect command line arguments!")
		os.Exit(2)
	}
	n, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Println(err)
		os.Exit(3)
	}
	fmt.Printf("%.17f\n", pi(n))
}
