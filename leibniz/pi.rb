#!/usr/bin/env ruby

def pi n
  t = 0
  (0 .. n - 1).each { |i|
    t += (i % 2 == 1 ? -1.0 : 1.0) / (2.0 * i + 1.0)
  }
  4.0 * t
end

puts "#{'%.17f' % pi(ARGV[0].to_i)}"
