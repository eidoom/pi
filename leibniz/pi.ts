#!/usr/bin/env -S deno run

function leibniz(n: number) {
    let t = 0
    for (let i = 0; i < n; i++) {
        t += (i % 2 == 1 ? -1 : 1) / (2 * i + 1);
    }
    return 4 * t
}

console.log(leibniz(parseInt(Deno.args[0])).toPrecision(18))
