fn pi(n: i32) -> f64 {
    let mut t = 0.;
    for i in 0..n {
        t += (if i % 2 == 1 { -1. } else { 1. }) / (2. * (i as f64) + 1.);
    }
    4. * t
}

fn main() {
    println!(
        "{:.17}",
        pi(std::env::args()
            .nth(1)
            .expect("No argument!")
            .parse::<i32>()
            .expect("Couldn't parse argument!"))
    );
}
