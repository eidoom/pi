const std = @import("std");

fn leibniz(n: usize) f64 {
    var t: f64 = 0.0;
    var i: usize = 0;
    while (i < n) : (i += 1) {
        const s: f64 = if (i % 2 == 1) -1.0 else 1.0;
        t += s / (2.0 * @intToFloat(f64, i) + 1.0);
    }
    return 4.0 * t;
}

pub fn main() void {

    var arg_it = std.process.args(); // https://ziglang.org/documentation/master/std/#std;process.args
    _ = arg_it.skip(); // https://github.com/ziglang/zig/blob/9a18db8a80c96d206297e865d203b2a7d8a803ba/test/cli.zig#L17
    var gpa = std.heap.GeneralPurposeAllocator(.{}){}; // https://ziglang.org/documentation/master/#Memory
    const alloc = gpa.allocator();
    const arg0 = arg_it.next(alloc); // https://ziglearn.org/chapter-2/#iterators
    const arg1 = arg0.?; // https://ziglang.org/documentation/master/#Optionals
    const arg2 = arg1 catch unreachable; // https://ziglang.org/documentation/master/#Errors
    const n = std.fmt.parseUnsigned(usize, arg2, 10) catch unreachable; // https://jeang3nie.codeberg.page/case-for-modern-language-pt1/

    const p = leibniz(n);
    std.debug.print("{d}\n", .{p});
}
