#!/usr/bin/env janet

(defn pi [n]
    (var t 0)
    (for i 0 n
        (+= t (/ (if (= 1 (% i 2)) -1 1) (inc (* 2 i)))))
    (* 4 t))

(print (pi (->> :args dyn 1 scan-number)))
