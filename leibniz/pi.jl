#!/usr/bin/env julia

using Printf

function pi(n::Int)::Float64
    t = 0
    for i = 0:n-1
        t += (i % 2 == 1 ? -1 : 1) / (2 * i + 1)
    end
    4 * t
end

@printf "%.17f\n" pi(parse(Int, ARGS[1]))
