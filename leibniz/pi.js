#!/usr/bin/env node

function pi(n) {
    let t = 0
    for (let i = 0; i < n; i++) {
        t += (i % 2 == 1 ? -1 : 1) / (2 * i + 1);
    }
    return 4 * t
}

console.log(pi(parseInt(process.argv.slice(2)[0])).toPrecision(18))
