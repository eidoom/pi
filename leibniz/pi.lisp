#!/usr/bin/env -S sbcl --script

(require "asdf")

(defun leibniz (n)
    (let ((tot 0.0d0))
    (dotimes (i n)
        (incf tot (/ (if (= 1 (mod i 2)) -1 1) (+ 1 (* 2 i)))))
    (* 4 tot)))

(let ((n (parse-integer (first (uiop:command-line-arguments)))))
	(format t "~f~%" (leibniz n)))
