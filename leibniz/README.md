# [leibniz](https://gitlab.com/eidoom/pi/-/tree/master/leibniz)

## Usage
* Linux system assumed
* compile binaries for compiled implementations with `bench.py -c`
* run benchmarks with `./bench.py -r`
* feed `results.json` into [datavis](https://gitlab.com/eidoom/datavis)

## Implementation specification
* $ \pi = 4 sum_{n=0}^{\infty} \frac{{(-1)}^{n}}{2n+1}
* [Leibniz formula](https://en.wikipedia.org/wiki/Leibniz_formula_for_%CF%80)
* <https://en.wikipedia.org/wiki/Approximations_of_%CF%80#Gregory%E2%80%93Leibniz_series>
* `1 - ((n & 1) << 1)` `==` `i % 2 ? -1 : 1` `>` `pow(-1, i)`
    * All implementations must use the second one
* Answer must be a 64-bit floating point number (`double`)

## Notes
* <https://en.wikipedia.org/wiki/Execution_(computing)>
* <https://en.wikipedia.org/wiki/Programming_paradigm>
* `bench.sh` is a previous implementation of `bench.py`
* using TOML
    * [CLI `toml`](https://lib.rs/crates/toml-cli)
        * install: `cargo install toml-cli`
        * use eg: `toml get bench.toml c | jq`
    * Python `toml`: `pip3 install --user --upgrade toml`
* Some declarative implementations perform poorly due to lack of lazy iterators
    * i.e. creating an array and iterating over its entries is more expensive than a lazy generator that returns the next iteration each time it is called
* Optimal compiler optimisation settings should be used
