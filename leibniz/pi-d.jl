#!/usr/bin/env julia

using Printf

function pi(n::Int)::Float64
    4 * sum(i -> (i % 2 == 1 ? -1 : 1) / (2 * i + 1), 0:n-1)
end

@printf "%.17f\n" pi(parse(Int, ARGS[1]))
