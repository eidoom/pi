#!/usr/bin/env -S nim c -r -o:pi-nim

from os import paramStr
from strutils import parseInt
from strformat import fmt

proc pi(n: int): float =
    var t: float = 0.0

    for i in 0..n-1:
        t += (if i mod 2 == 1: -1 else: 1) / (2.0 * float(i) + 1.0)

    return 4.0 * t


echo fmt"{pi(parseInt(paramStr(1))):.17f}"
