#!/usr/bin/env node

const pi = n => {
    return 4 * [...Array(n).keys()].reduce((t, i) => t + (i % 2 == 1 ? -1 : 1) / (2 * i + 1), 0)
}

console.log(pi(parseInt(process.argv.slice(2)[0])).toPrecision(18))
