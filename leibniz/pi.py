#!/usr/bin/env python3

import sys


def pi(n):
    t = 0
    for i in range(n):
        t += (-1 if i % 2 else 1) / (2 * i + 1)
    return 4 * t


if __name__ == "__main__":
    print(f"{pi(int(sys.argv[1])):.17f}")
