#include <iostream>
#include <cassert>

double pi(const int n)
{
    double t { 0. };

    for (int i { 0 }; i < n; ++i) {
        t += (i % 2 == 1 ? -1. : 1.) / (2. * static_cast<double>(i) + 1.);
    }

    return 4. * t;
}

int main(int argc, char* argv[])
{
    std::cout.sync_with_stdio(false);
    std::cout.precision(18);
    assert(argc == 2);
    std::cout << pi(std::atoi(argv[1])) << '\n';
}
