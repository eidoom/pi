program main
    implicit none
    real :: pi
    character(len=16) :: arg
    integer :: n

    call get_command_argument(1, arg)
    read(arg, '(i16)') n
    print "(f0.17)", pi(n)
end program main

function pi(n) result(t)
    implicit none
    integer, intent(in) :: n

    real :: t
    integer :: i

    t = 0
    do i=0,n-1
        t = t + merge(-1, 1, mod(i, 2) == 1) / (2 * real(i) + 1)
    end do
    t = t * 4
end function pi
