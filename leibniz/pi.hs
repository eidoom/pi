#!/usr/bin/env runhaskell
import System.Environment (getArgs)
import Text.Printf (printf)

term :: Int -> Double
term i = do
  (if ((mod i 2) == 1)
     then -1.0
     else 1.0)
    / (fromIntegral i * 2.0 + 1.0)

main :: IO ()
main = do
  printf "%.17f\n"
    . (* 4.0)
    . sum
    . map term
    . (\n -> [0 .. n - 1])
    . read
    . head
    =<< getArgs
