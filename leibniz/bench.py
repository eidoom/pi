#!/usr/bin/env python3

import argparse
import datetime
import json
import platform
import subprocess
import time
import tomllib


def recursive_filter(data, blocked_fields):
    if isinstance(data, dict):
        return {
            k: recursive_filter(v, blocked_fields)
            for k, v in data.items()
            if k not in blocked_fields
        }
    if isinstance(data, list):
        return [recursive_filter(v, blocked_fields) for v in data]
    return data


def timer(cmd, n, r, show):
    ts = []
    for _ in range(r):
        one = time.perf_counter()
        subprocess.run([*cmd, str(n)], stdout=None if show else subprocess.DEVNULL)
        two = time.perf_counter()
        ts.append(two - one)
    return ts


def run(data, r, np0, np1, show):
    for language in data["languages"]:
        for implementation in language["implementations"]:
            try:
                compiler = implementation["compiler"]
                compiler["version"] = subprocess.getoutput(
                    f"{compiler['run']} {compiler['get_version']}"
                )
            except KeyError:
                pass
            try:
                interpreter = implementation["interpreter"]
                interpreter["version"] = subprocess.getoutput(
                    f"{interpreter['run']} {interpreter['get_version']}"
                )
            except KeyError:
                pass

    print(f"Running with {r} iterations each")
    for n in (10**i for i in range(np0, np1)):
        print(f"============\nn={n}\n============")
        for language in data["languages"]:
            print(language["name"].capitalize())
            for implementation in language["implementations"]:
                print("  " + implementation["name"])
                for paradigm in implementation["paradigms"]:
                    print("    " + paradigm["styles"][0])

                    if "interpreter" in implementation:
                        interpreter = implementation["interpreter"]
                        cmd = [interpreter["run"]]
                        if "flags" in interpreter:
                            cmd += interpreter["flags"].split()
                        if "compiler" not in implementation:
                            # interpreted
                            cmd += [paradigm["source"]]
                        else:
                            # bytecode
                            cmd += [
                                (
                                    paradigm["run"]
                                    if "run" in paradigm
                                    else paradigm["binary"]
                                )
                            ]
                    elif "compiler" in implementation:
                        # native
                        cmd = [f"./{paradigm['binary']}"]

                    if show:
                        print("      " + " ".join(cmd))

                    ts = timer(cmd, n, r, show)
                    a = sum(ts) / r
                    print(f"      t={a:.3f}s")
                    res = {"n": n, "times": ts}
                    try:
                        paradigm["bench"].append(res)
                    except KeyError:
                        paradigm["bench"] = [res]


def make(data):
    prgs = "PRGS="
    body = ""
    for language in data["languages"]:
        for implementation in language["implementations"]:
            for paradigm in implementation["paradigms"]:
                try:
                    compiler = implementation["compiler"]
                    prgs += paradigm["binary"] + " "
                    body += (
                        paradigm["binary"]
                        + ": "
                        + paradigm["source"]
                        + "\n"
                        + "\t"
                        + compiler["run"]
                        + " "
                        + compiler.get("flags", "")
                        + " "
                        + compiler["files"]
                        + "\n\n"
                    )
                except KeyError:
                    pass
    with open("Makefile", "w") as f:
        f.write(
            ".PHONY: all clean\n\n"
            + prgs
            + "\n\nall: ${PRGS}\n\nmake clean:\n\t-rm ${PRGS} *.o *.hi\n\n"
            + body
        )


def get_args():
    parser = argparse.ArgumentParser(
        "Compile (if appropriate) and run implementation benchmarks"
    )
    parser.add_argument(
        "-c",
        "--compile",
        action="store_true",
        help="Compile benchmarks",
    )
    parser.add_argument(
        "-r",
        "--run",
        action="store_true",
        help="Run benchmarks",
    )
    parser.add_argument(
        "-d",
        "--dry-run",
        action="store_true",
        help="Don't save the results to file",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Increase verbosity (show calculated values of pi and run commands)",
    )
    parser.add_argument(
        "-i",
        "--iterations",
        metavar="I",
        type=int,
        default=3,
        help="Number of run repetitions to average run time over for each implementation [default: 3]",
    )
    parser.add_argument(
        "-n",
        "--input-range",
        metavar="N",
        type=int,
        nargs=2,
        default=[3, 9],
        help="Run algorithm with M terms in the series for M in {10^N0, 10^(N0+1), ... 10^N1} [default: 3 9]",
    )
    parser.add_argument(
        "-l",
        "--languages",
        metavar="L",
        type=str,
        nargs="*",
        help="Which languages to include (whitelist), print available languages if no argument [default: all]",
    )
    parser.add_argument(
        "-b",
        "--blacklist",
        metavar="B",
        type=str,
        nargs="+",
        help="Which languages to exclude [default: none]",
    )
    args = parser.parse_args()
    if None not in (args.blacklist, args.languages) and (
        com := set(args.languages) & set(args.blacklist)
    ):
        exit(
            f"Whitelist and blacklist are exclusive: resolve ambiguities for {', '.join(com)}"
        )
    return args


if __name__ == "__main__":
    args = get_args()

    with open("bench.toml", "r") as f:
        data = tomllib.loads(f.read())

    if args.languages is not None:
        languages = [l["name"] for l in data["languages"]]

        if args.languages == []:
            print(sorted(languages))
            exit()

        for language in (l for l in args.languages if l not in languages):
            raise Exception(f"unrecognised language {language}")

        data["languages"] = [
            l for l in data["languages"] if l["name"] in args.languages
        ]

    if args.blacklist is not None:
        data["languages"] = [
            l for l in data["languages"] if l["name"] not in args.blacklist
        ]

    if args.compile:
        make(data)
        subprocess.run(["make", "-j"])

    if args.run:
        run(
            data,
            args.iterations,
            args.input_range[0],
            args.input_range[1],
            args.verbose,
        )

    if not args.dry_run:
        out = {
            "languages": recursive_filter(
                data["languages"], ["source", "binary", "files", "get_version", "run"]
            ),
            "time": datetime.datetime.now().isoformat(),
            "cpu": subprocess.getoutput(
                "cat /proc/cpuinfo | grep 'model name' | head -1 | cut -d':' -f2 | xargs"
            ),
            "os": platform.freedesktop_os_release()["PRETTY_NAME"],
            "iterations": args.iterations,
        }

        with open("results.json", "w") as f:
            f.write(json.dumps(out))
