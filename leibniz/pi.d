double pi(const int n)
{
    double t = 0.;

    for (int i = 0; i < n; ++i) {
        t += (i % 2 == 1 ? -1. : 1.) / (2. * cast(double) i + 1.);
    }

    return 4. * t;
}

void main(string[] args)
{
    import std.stdio : writefln;
    import std.conv : parse;

    assert(args.length == 2);
    writefln("%.17f", pi(args[1].parse!int));
}
