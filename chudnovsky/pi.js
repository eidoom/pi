#!/usr/bin/env node

function calc_pi() {
    const C = 426880 * Math.sqrt(10005)

    let L = 13591409
    let X = 1
    let K = 6
    let M = 1
    let s = L

    for (let q = 1; q < 3; q++) {
        L += 545140134
        X *= -262537412640768000
        M *= (K ** 3 - 16 * K) / q ** 3
        K += 12
        s += M * L / X
    }

    return C / s
}


console.log(calc_pi().toFixed(64))
