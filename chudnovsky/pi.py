#!/usr/bin/env python3

import math


def calc_pi():
    C = 426880 * math.sqrt(10005)

    L = 13591409
    X = 1
    K = 6
    M = 1
    s = L

    for q in range(1, 3):
        L += 545140134
        X *= -262537412640768000
        M *= (K ** 3 - 16 * K) // q ** 3
        K += 12
        s += M * L / X

    return C / s


print(f"{calc_pi():.64f}")
