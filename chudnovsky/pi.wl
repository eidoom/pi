calcPi[]:=Module[{l, x, k, m, s, c},
    c = 426880 * Sqrt[10005.];

    l = 13591409;
    x = 1;
    k = 6;
    m = 1;
    s = l;

    Do[
        {
            l += 545140134;
            x *= -262537412640768000;
            m *= (k^3 - 16 * k) / q^3;
            k += 12;
            s += m * l / x;
        },
        {q, 2}
    ];

    Return[c / s];
];


Print[SetPrecision[calcPi[], 64]];
