#!/usr/bin/env -S nim c -r -o:pi-nim

import math

proc calc_pi(): float =
    const C = 426880.0 * sqrt(10005.0)

    var L: int64 = 13591409
    var X: int64 = 1
    var K: int64 = 6
    var M: int64 = 1
    var s = float(L)

    for q in int64(1)..1:
        L += 545140134
        X *= -262537412640768000
        M *= (K^3 - 16 * K) div q^3
        K += 12
        s += float(M * L) / float(X)

    return C / s


echo calc_pi()
