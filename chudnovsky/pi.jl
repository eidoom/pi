#!/usr/bin/env julia

function calc_pi()::BigFloat
    C::BigFloat = 426880 * sqrt(10005)

    L::BigInt = 13591409
    X::BigInt = 1
    K::BigInt = 6
    M::BigInt = 1
    s::BigFloat = L

    for q::BigInt = 1:2
        L += 545140134
        X *= -262537412640768000
        M *= div(K^3 - 16 * K, q^3)
        K += 12
        s += M * L / X
    end

    C / s
end

println(calc_pi())
