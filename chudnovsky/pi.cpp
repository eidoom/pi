#include <cmath>
#include <iostream>

long long inline cube(long long x)
{
    return x * x * x;
}

long double calc_pi()
{
    const long double c { 426880 * std::sqrt(10005) };

    long long l { 13591409 };
    long long x { 1 };
    long long k { 6 };
    long long m { 1 };
    long double s { static_cast<long double>(l) };

    for (long long q { 1 }; q < 3; ++q) {
        l += 545140134;
        x *= -262537412640768000;
        m *= (cube(k) - 16 * k) / cube(q);
        k += 12;
        s += static_cast<long double>(m * l) / x;
    }

    return c / s;
}

int main()
{
    std::cout.sync_with_stdio(false);
    std::cout.precision(64);
    std::cout << calc_pi() << '\n';
}
