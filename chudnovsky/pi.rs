fn calc_pi() -> f64 {
    let c: f64 = 426880f64 * (10005f64).sqrt();

    let mut l: i128 = 13591409;
    let mut x: i128 = 1;
    let mut k: i128 = 6;
    let mut m: i128 = 1;
    let mut s: f64 = l as f64;

    for q in 1i128..3 {
        l += 545140134;
        x *= -262537412640768000;
        m *= (k.pow(3) - 16 * k) / q.pow(3);
        k += 12;
        s += ((m * l) as f64) / (x as f64);
    }

    c / s
}

fn main() {
    println!("{:.64}", calc_pi());
}
